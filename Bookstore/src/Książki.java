import net.proteanit.sql.DbUtils;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;

import javax.swing.*;
import java.awt.*;
import java.sql.*;


public class Książki {


    public void książka() throws SQLException, ClassNotFoundException{
        DatabaseConnection databaseConnection = DatabaseConnection.getInstance();
        String[][] dane = databaseConnection.getKsiazki();
        String[] nazwyKolumn = {"ID", "tytuł", "nazwa_wydawnictwa", "gatunek", "rok_wydania"};
        final JTable[] tabela = {new JTable(dane, nazwyKolumn)};
        final JScrollPane[] scrollPane = {new JScrollPane(tabela[0])};
        tabela[0].setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        for (int i = 0; i < tabela[0].getColumnCount(); i++){
            tabela[0].getColumnModel().getColumn(i).setPreferredWidth(130);
        }
        tabela[0].setPreferredScrollableViewportSize(new Dimension(tabela[0].getPreferredSize().width, 300));
        scrollPane[0].getVerticalScrollBar().setPreferredSize(new Dimension(20, Integer.MAX_VALUE));
        JFrame frame = new JFrame("Książki");
        JPanel panel = new JPanel();
        JPanel buttonPanel = new JPanel();
        JPanel przyciskPanel = new JPanel(new GridLayout(2, 3));
        JPanel szukajPanel = new JPanel();

        JButton powrotButton = new JButton("Powrót");
        JButton dodajButton = new JButton("Dodaj");
        JButton aktualizujButton = new JButton("Zmień");
        JButton usunButton = new JButton("Usuń");
        JButton znajdzButton = new JButton("Znajdź");
        JTextField znajdzTekst = new JTextField("", 20);
        JLabel znajdz = new JLabel("Wpisz tytuł");

        szukajPanel.add(znajdz);
        szukajPanel.add(znajdzTekst);
        szukajPanel.add(znajdzButton);

        buttonPanel.add(powrotButton);
        buttonPanel.add(dodajButton);
        buttonPanel.add(aktualizujButton);
        buttonPanel.add(usunButton);

        przyciskPanel.add(szukajPanel);
        przyciskPanel.add(buttonPanel);
        panel.add(przyciskPanel);
        panel.add(scrollPane[0]);

        powrotButton.addActionListener(e ->
        {
            frame.dispose();
            SwingUtilities.invokeLater(() -> new Main().Ekran());
        });

        znajdzButton.addActionListener(e ->
        {
            panel.remove(scrollPane[0]);
            JScrollPane scrollPaneAktualizacja = new JScrollPane(tabela[0]);
            JTable tabelaAktualizacja = new JTable(dane, nazwyKolumn);
            try {
                String tekst = znajdzTekst.getText();
                tabelaAktualizacja.setModel(DbUtils.resultSetToTableModel(znajdzKsiążkę(tekst)));
                scrollPaneAktualizacja = new JScrollPane(tabelaAktualizacja);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            } catch (ClassNotFoundException classNotFoundException) {
                classNotFoundException.printStackTrace();
            }
            scrollPane[0] = scrollPaneAktualizacja;
            scrollPane[0].getVerticalScrollBar().setPreferredSize(new Dimension(20, Integer.MAX_VALUE));
            tabela[0] = tabelaAktualizacja;
            tabela[0].setPreferredScrollableViewportSize(new Dimension(tabela[0].getPreferredSize().width, 300));
            panel.add(scrollPane[0]);
            panel.updateUI();
            frame.repaint();
        });

        dodajButton.addActionListener(e ->
        {
            panel.remove(scrollPane[0]);
            JScrollPane scrollPaneUpdate = new JScrollPane(tabela[0]);
            JTable tabelaAktualizacja = new JTable(dane, nazwyKolumn);
            try {
                dodajKsiążkę();
                String [][] daneAktualizacja = databaseConnection.getKsiazki();
                tabelaAktualizacja = new JTable(daneAktualizacja, nazwyKolumn);
                scrollPaneUpdate = new JScrollPane(tabelaAktualizacja);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            } catch (ClassNotFoundException classNotFoundException) {
                classNotFoundException.printStackTrace();
            }
            scrollPane[0] = scrollPaneUpdate;
            scrollPane[0].getVerticalScrollBar().setPreferredSize(new Dimension(20, Integer.MAX_VALUE));
            tabela[0] = tabelaAktualizacja;
            tabela[0].setPreferredScrollableViewportSize(new Dimension(tabela[0].getPreferredSize().width, 300));
            panel.add(scrollPane[0]);
            panel.updateUI();
            frame.repaint();

        });

        aktualizujButton.addActionListener(e ->
        {
            panel.remove(scrollPane[0]);
            JScrollPane scrollPaneUpdate = new JScrollPane(tabela[0]);
            JTable tabelaAktualizacja = new JTable(dane, nazwyKolumn);
            try {
                int rekord = tabela[0].getSelectedRow();
                Object old_id_ks = tabela[0].getValueAt(rekord, 0);
                Object old_tytul = tabela[0].getValueAt(rekord, 1);
                Object old_nazwa = tabela[0].getValueAt(rekord, 2);
                Object old_gatunek = tabela[0].getValueAt(rekord, 3);
                Object old_rok= tabela[0].getValueAt(rekord, 4);
                aktualizujKsiążkę(old_id_ks, old_tytul, old_nazwa, old_gatunek, old_rok);
                String [][] zaaktualizowaneDane = databaseConnection.getKsiazki();
                tabelaAktualizacja = new JTable(zaaktualizowaneDane, nazwyKolumn);
                scrollPaneUpdate = new JScrollPane(tabelaAktualizacja);


            } catch (SQLException throwables) {
                throwables.printStackTrace();
            } catch (ClassNotFoundException classNotFoundException) {
                classNotFoundException.printStackTrace();
            }
            scrollPane[0] = scrollPaneUpdate;
            scrollPane[0].getVerticalScrollBar().setPreferredSize(new Dimension(20, Integer.MAX_VALUE));
            tabela[0] = tabelaAktualizacja;
            tabela[0].setPreferredScrollableViewportSize(new Dimension(tabela[0].getPreferredSize().width, 300));
            panel.add(scrollPane[0]);
            panel.updateUI();
            frame.repaint();

        });

        usunButton.addActionListener(e ->
        {
            int rekord = tabela[0].getSelectedRow();
            Object id = tabela[0].getValueAt(rekord, 0);
            panel.remove(scrollPane[0]);
            JScrollPane scrollPaneUpdate = new JScrollPane(tabela[0]);
            JTable tabelaAktualizacja = new JTable(dane, nazwyKolumn);
            try {
                usunKsiążkę(id);
                String [][] daneAktualizacja = databaseConnection.getKsiazki();
                tabelaAktualizacja = new JTable(daneAktualizacja, nazwyKolumn);
                scrollPaneUpdate = new JScrollPane(tabelaAktualizacja);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            } catch (ClassNotFoundException classNotFoundException) {
                classNotFoundException.printStackTrace();
            }
            scrollPane[0] = scrollPaneUpdate;
            scrollPane[0].getVerticalScrollBar().setPreferredSize(new Dimension(20, Integer.MAX_VALUE));
            tabela [0] = tabelaAktualizacja;
            panel.add(scrollPane[0]);
            panel.updateUI();
            frame.repaint();

        });

        frame.add(panel);
        frame.setMinimumSize(new Dimension(700, 600));
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

    ResultSet znajdzKsiążkę(String tytul) throws SQLException, ClassNotFoundException{
        try {
            DatabaseConnection databaseConnection = DatabaseConnection.getInstance();

            PreparedStatement preparedStatement;
            String query = "SELECT * FROM Książki WHERE tytuł = ?";
            preparedStatement = databaseConnection.getConnection().prepareStatement(query);
            preparedStatement.setString(1, tytul);
            return preparedStatement.executeQuery();

        }
        catch (SQLException err) {
            JOptionPane.showMessageDialog(null, "Błąd SQL");
        }
        return null;
    }



    public void dodajKsiążkę() throws SQLException, ClassNotFoundException{

        DatabaseConnection databaseConnection = DatabaseConnection.getInstance();
        String[] wydawnictwa = databaseConnection.getWyd();

        JTextField tytulField = new JTextField();
        JComboBox <String> wybierzWydawnictwo = new JComboBox(wydawnictwa);
        wybierzWydawnictwo.setSelectedItem("");
        AutoCompleteDecorator.decorate(wybierzWydawnictwo);
        JTextField gatunekField = new JTextField();
        JTextField rokField = new JTextField();
        JPanel okienko = new JPanel(new GridLayout( 0, 1));

        okienko.add(new JLabel("Tytuł książki"));
        okienko.add(tytulField);
        okienko.add(new JLabel("Nazwa wydawnictwa"));
        okienko.add(wybierzWydawnictwo);
        okienko.add(new JLabel("Gatunek książki"));
        okienko.add(gatunekField);
        okienko.add(new JLabel("Rok wydania"));
        okienko.add(rokField);

        int wynik = JOptionPane.showConfirmDialog(null, okienko, "Dodaj pracownika ",
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if(wynik == JOptionPane.OK_OPTION)
        {
            try {
                int  rok;
                String tytul, nazwa_wydawnictwa, gatunek;
                if (!((tytulField.getText().equals("")) && (wybierzWydawnictwo.getSelectedItem().equals("")) &&
                        (gatunekField.getText().equals("")) && (Integer.parseInt(rokField.getText())) == 0)) {

                    tytul = tytulField.getText();
                    nazwa_wydawnictwa = wybierzWydawnictwo.getSelectedItem().toString();
                    gatunek = gatunekField.getText();
                    rok = Integer.parseInt(rokField.getText());



                    PreparedStatement preparedStatement;
                    String query = "INSERT INTO Książki VALUES (?, ?, ?, ?, ?)";
                    preparedStatement = databaseConnection.getConnection().prepareStatement(query);
                    preparedStatement.setString(2, tytul);
                    preparedStatement.setString(3, nazwa_wydawnictwa);
                    preparedStatement.setString(4, gatunek);
                    preparedStatement.setDouble(5, rok);
                    preparedStatement.executeUpdate();

                }

            }
            catch (SQLException err) {
                JOptionPane.showMessageDialog(null, "Błąd SQL");
            }
        }
        else{
            System.out.println("Anulowano");

        }
    }

    public void aktualizujKsiążkę(Object old_id, Object old_tytul, Object old_nazw, Object old_gatunek, Object old_rok) throws SQLException, ClassNotFoundException {
        JTextField tytulField = new JTextField(old_tytul.toString());
        JTextField nazwa_wydawnictwaField = new JTextField(old_nazw.toString());
        JTextField gatunekField = new JTextField(old_gatunek.toString());
        JTextField rok_wydaniaField = new JTextField(old_rok.toString());
        JPanel okienko = new JPanel(new GridLayout( 0, 1));

        okienko.add(new JLabel("Tytuł książki"));
        okienko.add(tytulField);
        okienko.add(new JLabel("Nazwa wydawnictwa"));
        okienko.add(nazwa_wydawnictwaField);
        okienko.add(new JLabel("Nazwa gatunku"));
        okienko.add(gatunekField);
        okienko.add(new JLabel("Rok wydania"));
        okienko.add(rok_wydaniaField);

        int wynik = JOptionPane.showConfirmDialog(null, okienko, "Dodaj pracownika ",
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if(wynik == JOptionPane.OK_OPTION)
        {
            try {
                String tytul, nazwa_wydawnictwa, gatunek;
                int rok_wydania;
                if (!(tytulField.getText().equals("") && nazwa_wydawnictwaField.getText().equals("")
                        && gatunekField.getText().equals("") && Integer.parseInt(rok_wydaniaField.getText()) == 0)) {
                    tytul = tytulField.getText();
                    nazwa_wydawnictwa = nazwa_wydawnictwaField.getText();
                    gatunek= gatunekField.getText();
                    rok_wydania = Integer.parseInt(rok_wydaniaField.getText());

                    DatabaseConnection databaseConnection = DatabaseConnection.getInstance();

                    PreparedStatement preparedStatement;
                    String query = "UPDATE Książki SET tytuł = ?, nazwa_wydawnictwa = ?, gatunek = ?, rok_wydania = ? WHERE id_książki = "+ old_id;
                    preparedStatement = databaseConnection.getConnection().prepareStatement(query);
                    preparedStatement.setString(1, tytul);
                    preparedStatement.setString(2, nazwa_wydawnictwa);
                    preparedStatement.setString(3, gatunek);
                    preparedStatement.setDouble(4, rok_wydania);
                    preparedStatement.executeUpdate();
                }

            }
            catch (SQLException err) {
                JOptionPane.showMessageDialog(null, "Błąd SQL");
            }
        }
        else{
            System.out.println("Anulowano");

        }
    }
    public void usunKsiążkę(Object id) throws SQLException, ClassNotFoundException{
        DatabaseConnection databaseConnection = DatabaseConnection.getInstance();

        PreparedStatement preparedStatement;
        String query = "DELETE FROM Książki WHERE id_książki = "+ id;
        preparedStatement = databaseConnection.getConnection().prepareStatement(query);
        preparedStatement.executeUpdate();
    }
}