
import javax.swing.*;
import java.awt.*;
import java.sql.*;

public class Main {
    public static void main(String[] args)
    {
        SwingUtilities.invokeLater(() -> new Main().Ekran());
    }

    public void Ekran(){

        JFrame frame = new JFrame("Panel");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JPanel panel = new JPanel();
        JPanel dolnyPanel = new JPanel();
        dolnyPanel.setOpaque(false);

        JButton pracownicyButton = new JButton("Pracownicy");
        JButton wypożyczającyButton = new JButton("Wypożyczający");
        JButton wypożyczeniaButton = new JButton("Wypożyczenia");
        JButton wydawnictwoButton = new JButton("Wydawnictwa");
        JButton książkiButton = new JButton("Książki");

        dolnyPanel.add(pracownicyButton);
        dolnyPanel.add(wypożyczającyButton);
        dolnyPanel.add(wydawnictwoButton);
        dolnyPanel.add(książkiButton);
        dolnyPanel.add(wypożyczeniaButton);


        panel.add(dolnyPanel, BorderLayout.PAGE_END);

        pracownicyButton.addActionListener(e -> {
            Pracownicy pracownicy = new Pracownicy();
            try {
                pracownicy.pracownik();
                frame.dispose();
            }
            catch (SQLException | ClassNotFoundException throwables)
            {
                throwables.printStackTrace();
            }
        });

        wypożyczającyButton.addActionListener(e -> {
            Wypozyczajacy wypozyczajacy = new Wypozyczajacy();
            try {
                wypozyczajacy.wypozyczajacy();
                frame.dispose();
            }
            catch (SQLException | ClassNotFoundException throwables)
            {
                throwables.printStackTrace();
            }
        });

        wypożyczeniaButton.addActionListener(e -> {
            Wypożyczenia wypożyczenia = new Wypożyczenia();
            try {
                wypożyczenia.wypożyczenie();
                frame.dispose();
            }
            catch (SQLException | ClassNotFoundException throwables)
            {
                throwables.printStackTrace();
            }
        });

        wydawnictwoButton.addActionListener(e -> {
            Wydawnictwo wydawnictwo = new Wydawnictwo();
            try {
                wydawnictwo.wydawnictwo();
                frame.dispose();
            }
            catch (SQLException | ClassNotFoundException throwables)
            {
                throwables.printStackTrace();
            }
        });

        książkiButton.addActionListener(e -> {
            Książki książki = new Książki();
            try {
                książki.książka();
                frame.dispose();
            }
            catch (SQLException | ClassNotFoundException throwables)
            {
                throwables.printStackTrace();
            }
        });

        frame.add(panel);
        frame.setMinimumSize(new Dimension(600, 400));
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
