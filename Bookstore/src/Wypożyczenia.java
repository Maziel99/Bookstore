import net.proteanit.sql.DbUtils;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;

import javax.swing.*;
import java.awt.*;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;


public class Wypożyczenia {

    public void wypożyczenie() throws SQLException, ClassNotFoundException{
        DatabaseConnection databaseConnection = DatabaseConnection.getInstance();
        String[][] dane = databaseConnection.getWypozyczenia();
        String[] nazwyKolumn = {"ID", "wypożyczający", "tytuł", "pracownik", "data_wypożyczenia"};
        final JTable[] tabela = {new JTable(dane, nazwyKolumn)};
        final JScrollPane[] scrollPane = {new JScrollPane(tabela[0])};
        tabela[0].setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        for (int i = 0; i < tabela[0].getColumnCount(); i++){
            tabela[0].getColumnModel().getColumn(i).setPreferredWidth(130);
        }
        tabela[0].setPreferredScrollableViewportSize(new Dimension(tabela[0].getPreferredSize().width, 300));
        scrollPane[0].getVerticalScrollBar().setPreferredSize(new Dimension(20, Integer.MAX_VALUE));
        JFrame frame = new JFrame("Wypożyczenia");
        JPanel panel = new JPanel();
        JPanel buttonPanel = new JPanel();
        JPanel przyciskPanel = new JPanel(new GridLayout(2, 3));
        JPanel szukajPanel = new JPanel();

        JButton powrotButton = new JButton("Powrót");
        JButton dodajButton = new JButton("Dodaj");
        JButton usunButton = new JButton("Usuń");
        JButton znajdzButton = new JButton("Znajdź");
        JTextField znajdzTekst = new JTextField("", 20);
        JLabel znajdz = new JLabel("Wpisz wypożyczającego");

        szukajPanel.add(znajdz);
        szukajPanel.add(znajdzTekst);
        szukajPanel.add(znajdzButton);

        buttonPanel.add(powrotButton);
        buttonPanel.add(dodajButton);
        buttonPanel.add(usunButton);

        przyciskPanel.add(szukajPanel);
        przyciskPanel.add(buttonPanel);
        panel.add(przyciskPanel);
        panel.add(scrollPane[0]);

        powrotButton.addActionListener(e ->
        {
            frame.dispose();
            SwingUtilities.invokeLater(() -> new Main().Ekran());
        });

        znajdzButton.addActionListener(e ->
        {
            panel.remove(scrollPane[0]);
            JScrollPane scrollPaneAktualizacja = new JScrollPane(tabela[0]);
            JTable tabelaAktualizacja = new JTable(dane, nazwyKolumn);
            try {
                String tekst = znajdzTekst.getText();
                tabelaAktualizacja.setModel(DbUtils.resultSetToTableModel(znajdzWypożyczenie(tekst)));
                scrollPaneAktualizacja = new JScrollPane(tabelaAktualizacja);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            } catch (ClassNotFoundException classNotFoundException) {
                classNotFoundException.printStackTrace();
            }
            scrollPane[0] = scrollPaneAktualizacja;
            scrollPane[0].getVerticalScrollBar().setPreferredSize(new Dimension(20, Integer.MAX_VALUE));
            tabela[0] = tabelaAktualizacja;
            tabela[0].setPreferredScrollableViewportSize(new Dimension(tabela[0].getPreferredSize().width, 300));
            panel.add(scrollPane[0]);
            panel.updateUI();
            frame.repaint();
        });

        dodajButton.addActionListener(e ->
        {
            panel.remove(scrollPane[0]);
            JScrollPane scrollPaneUpdate = new JScrollPane(tabela[0]);
            JTable tabelaAktualizacja = new JTable(dane, nazwyKolumn);
            try {
                dodajWypożyczenie();
                String [][] daneAktualizacja = databaseConnection.getWypozyczenia();
                tabelaAktualizacja = new JTable(daneAktualizacja, nazwyKolumn);
                scrollPaneUpdate = new JScrollPane(tabelaAktualizacja);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            } catch (ClassNotFoundException classNotFoundException) {
                classNotFoundException.printStackTrace();
            }
            scrollPane[0] = scrollPaneUpdate;
            scrollPane[0].getVerticalScrollBar().setPreferredSize(new Dimension(20, Integer.MAX_VALUE));
            tabela[0] = tabelaAktualizacja;
            panel.add(scrollPane[0]);
            panel.updateUI();
            frame.repaint();

        });


        usunButton.addActionListener(e ->
        {
            int rekord = tabela[0].getSelectedRow();
            Object id = tabela[0].getValueAt(rekord, 0);
            panel.remove(scrollPane[0]);
            JScrollPane scrollPaneUpdate = new JScrollPane(tabela[0]);
            JTable tabelaAktualizacja = new JTable(dane, nazwyKolumn);
            try {
                usunWypożyczenie(id);
                String [][] daneAktualizacja = databaseConnection.getWypozyczenia();
                tabelaAktualizacja = new JTable(daneAktualizacja, nazwyKolumn);
                scrollPaneUpdate = new JScrollPane(tabelaAktualizacja);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            } catch (ClassNotFoundException classNotFoundException) {
                classNotFoundException.printStackTrace();
            }
            scrollPane[0] = scrollPaneUpdate;
            scrollPane[0].getVerticalScrollBar().setPreferredSize(new Dimension(20, Integer.MAX_VALUE));
            tabela [0] = tabelaAktualizacja;
            panel.add(scrollPane[0]);
            panel.updateUI();
            frame.repaint();

        });

        frame.add(panel);
        frame.setMinimumSize(new Dimension(700, 600));
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

    ResultSet znajdzWypożyczenie(String tytul) throws SQLException, ClassNotFoundException{
        try {
            DatabaseConnection databaseConnection = DatabaseConnection.getInstance();

            PreparedStatement preparedStatement;
            String query = "SELECT * FROM Wypozyczenia WHERE wypożyczający = ?";
            preparedStatement = databaseConnection.getConnection().prepareStatement(query);
            preparedStatement.setString(1, tytul);
            return preparedStatement.executeQuery();

        }
        catch (SQLException err) {
            JOptionPane.showMessageDialog(null, "Błąd SQL");
        }
        return null;
    }



    public void dodajWypożyczenie() throws SQLException, ClassNotFoundException{

        DatabaseConnection databaseConnection = DatabaseConnection.getInstance();
        String[] wypozyczajacy = databaseConnection.getWyp();
        String[] tytuly = databaseConnection.getTyt();
        String[] pracownicy = databaseConnection.getPrac();


        JComboBox<String> wybierzWypozyczajego = new JComboBox(wypozyczajacy);
        wybierzWypozyczajego.setSelectedItem("");
        JComboBox<String> wybierzTytul = new JComboBox(tytuly);
        wybierzTytul.setSelectedItem("");
        JComboBox<String> wybierzPracownika = new JComboBox(pracownicy);
        wybierzPracownika.setSelectedItem("");
        AutoCompleteDecorator.decorate(wybierzWypozyczajego);
        AutoCompleteDecorator.decorate(wybierzTytul);
        AutoCompleteDecorator.decorate(wybierzPracownika);
        DateFormat format = new SimpleDateFormat("yyyy.MM.dd");
        JFormattedTextField dateField = new JFormattedTextField(format);
        JPanel okienko = new JPanel(new GridLayout( 0, 1));

        okienko.add(new JLabel("Nazwisko wypożyczającego"));
        okienko.add(wybierzWypozyczajego);
        okienko.add(new JLabel("Tytuł książki"));
        okienko.add(wybierzTytul);
        okienko.add(new JLabel("Nazwisko pracownika"));
        okienko.add(wybierzPracownika);
        okienko.add(new JLabel("Data wypożyczenia (rok-miesiąc-dzień)"));
        okienko.add(dateField);

        int wynik = JOptionPane.showConfirmDialog(null, okienko, "Dodaj wypożyczenie ",
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if(wynik == JOptionPane.OK_OPTION)
        {
            try {
                String wypozycz, tytul, pracownik, data;
                if (!((wybierzWypozyczajego.getSelectedItem().equals("")) && (wybierzTytul.getSelectedItem().equals("")) &&
                        (wybierzPracownika.getSelectedItem().equals("")) &&(dateField.getText().equals("")))) {
                    wypozycz = wybierzWypozyczajego.getSelectedItem().toString();;
                    tytul = wybierzTytul.getSelectedItem().toString();
                    pracownik = wybierzPracownika.getSelectedItem().toString();
                    data = dateField.getText();



                    PreparedStatement preparedStatement;
                    String query = "INSERT INTO Wypozyczenia VALUES (?, ?, ?, ?, ?)";
                    preparedStatement = databaseConnection.getConnection().prepareStatement(query);
                    preparedStatement.setString(2, wypozycz);
                    preparedStatement.setString(3, tytul);
                    preparedStatement.setString(4, pracownik);
                    preparedStatement.setString(5, data);
                    preparedStatement.executeUpdate();

                }

            }
            catch (SQLException err) {
                JOptionPane.showMessageDialog(null, "Błąd SQL");
            }
        }
        else{
            System.out.println("Anulowano");

        }
    }

    void usunWypożyczenie(Object id) throws SQLException, ClassNotFoundException{
        DatabaseConnection databaseConnection = DatabaseConnection.getInstance();

        PreparedStatement preparedStatement;
        String query = "DELETE FROM Wypozyczenia WHERE id_wypoz = "+ id;
        preparedStatement = databaseConnection.getConnection().prepareStatement(query);
        preparedStatement.executeUpdate();
    }
}