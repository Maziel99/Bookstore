import net.proteanit.sql.DbUtils;

import javax.swing.*;
import java.awt.*;
import java.sql.*;

public class Pracownicy {
    public Statement stmt;

    public  void pracownik() throws SQLException, ClassNotFoundException{
        DatabaseConnection databaseConnection = DatabaseConnection.getInstance();
        String[][] dane = databaseConnection.getPracownicy();
        String[] nazwyKolumn = {"ID", "imię", "nazwisko", "data_zatrudnienia", "zarobki", "numer_telefonu"};
        final JTable[] tabela = {new JTable(dane, nazwyKolumn)};
        final JScrollPane[] scrollPane = {new JScrollPane(tabela[0])};
        tabela[0].setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        for (int i = 0; i < tabela[0].getColumnCount(); i++){
            tabela[0].getColumnModel().getColumn(i).setPreferredWidth(100);
        }
        tabela[0].setPreferredScrollableViewportSize(new Dimension(tabela[0].getPreferredSize().width, 300));
        scrollPane[0].getVerticalScrollBar().setPreferredSize(new Dimension(20, Integer.MAX_VALUE));
        JFrame frame = new JFrame("Pracownicy");
        JPanel panel = new JPanel();
        JPanel buttonPanel = new JPanel();
        JPanel przyciskPanel = new JPanel(new GridLayout(2, 3));
        JPanel szukajPanel = new JPanel();

        JButton powrotButton = new JButton("Powrót");
        JButton dodajButton = new JButton("Dodaj");
        JButton aktualizujButton = new JButton("Zmień");
        JButton usunButton = new JButton("Usuń");
        JButton znajdzButton = new JButton("Znajdź");
        JTextField znajdzTekst = new JTextField("", 20);
        JLabel znajdz = new JLabel("Wpisz nazwisko");

        szukajPanel.add(znajdz);
        szukajPanel.add(znajdzTekst);
        szukajPanel.add(znajdzButton);

        buttonPanel.add(powrotButton);
        buttonPanel.add(dodajButton);
        buttonPanel.add(aktualizujButton);
        buttonPanel.add(usunButton);

        przyciskPanel.add(szukajPanel);
        przyciskPanel.add(buttonPanel);
        panel.add(przyciskPanel);
        panel.add(scrollPane[0]);

        powrotButton.addActionListener(e ->
        {
            frame.dispose();
            SwingUtilities.invokeLater(() -> new Main().Ekran());
        });

        znajdzButton.addActionListener(e ->
        {
            panel.remove(scrollPane[0]);
            JScrollPane scrollPaneAktualizacja = new JScrollPane(tabela[0]);
            JTable tabelaAktualizacja = new JTable(dane, nazwyKolumn);
            try {
                String tekst = znajdzTekst.getText();
                tabelaAktualizacja.setModel(DbUtils.resultSetToTableModel(znajdzPracownika(tekst)));
                scrollPaneAktualizacja = new JScrollPane(tabelaAktualizacja);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            } catch (ClassNotFoundException classNotFoundException) {
                classNotFoundException.printStackTrace();
            }
            scrollPane[0] = scrollPaneAktualizacja;
            scrollPane[0].getVerticalScrollBar().setPreferredSize(new Dimension(20, Integer.MAX_VALUE));
            tabela[0] = tabelaAktualizacja;
            tabela[0].setPreferredScrollableViewportSize(new Dimension(tabela[0].getPreferredSize().width, 300));
            panel.add(scrollPane[0]);
            panel.updateUI();
            frame.repaint();
        });

        dodajButton.addActionListener(e ->
        {
            panel.remove(scrollPane[0]);
            JScrollPane scrollPaneUpdate = new JScrollPane(tabela[0]);
            JTable tabelaAktualizacja = new JTable(dane, nazwyKolumn);
            try {
                dodajPracownika();
                String [][] daneAktualizacja = databaseConnection.getPracownicy();
                tabelaAktualizacja = new JTable(daneAktualizacja, nazwyKolumn);
                scrollPaneUpdate = new JScrollPane(tabelaAktualizacja);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            } catch (ClassNotFoundException classNotFoundException) {
                classNotFoundException.printStackTrace();
            }
            scrollPane[0] = scrollPaneUpdate;
            scrollPane[0].getVerticalScrollBar().setPreferredSize(new Dimension(20, Integer.MAX_VALUE));
            tabela[0] = tabelaAktualizacja;
            tabela[0].setPreferredScrollableViewportSize(new Dimension(tabela[0].getPreferredSize().width, 300));
            panel.add(scrollPane[0]);
            panel.updateUI();
            frame.repaint();

        });

        aktualizujButton.addActionListener(e ->
        {
            panel.remove(scrollPane[0]);
            JScrollPane scrollPaneUpdate = new JScrollPane(tabela[0]);
            JTable tabelaAktualizacja = new JTable(dane, nazwyKolumn);
            try {
                int rekord = tabela[0].getSelectedRow();
                Object old_id_prac = tabela[0].getValueAt(rekord, 0);
                Object old_imie = tabela[0].getValueAt(rekord, 1);
                Object old_nazwisko = tabela[0].getValueAt(rekord, 2);
                Object old_data = tabela[0].getValueAt(rekord, 3);
                Object old_zarobki = tabela[0].getValueAt(rekord, 4);
                Object old_telefon = tabela[0].getValueAt(rekord, 5);
                aktualizujPracownika(old_id_prac, old_imie, old_nazwisko, old_data, old_zarobki, old_telefon);
                String [][] zaaktualizowaneDane = databaseConnection.getPracownicy();
                tabelaAktualizacja = new JTable(zaaktualizowaneDane, nazwyKolumn);
                scrollPaneUpdate = new JScrollPane(tabelaAktualizacja);


            } catch (SQLException throwables) {
                throwables.printStackTrace();
            } catch (ClassNotFoundException classNotFoundException) {
                classNotFoundException.printStackTrace();
            }
            scrollPane[0] = scrollPaneUpdate;
            scrollPane[0].getVerticalScrollBar().setPreferredSize(new Dimension(20, Integer.MAX_VALUE));
            tabela[0] = tabelaAktualizacja;
            tabela[0].setPreferredScrollableViewportSize(new Dimension(tabela[0].getPreferredSize().width, 300));
            panel.add(scrollPane[0]);
            panel.updateUI();
            frame.repaint();

        });

        usunButton.addActionListener(e ->
        {
            int rekord = tabela[0].getSelectedRow();
            Object id = tabela[0].getValueAt(rekord, 0);
            panel.remove(scrollPane[0]);
            JScrollPane scrollPaneUpdate = new JScrollPane(tabela[0]);
            JTable tabelaAktualizacja = new JTable(dane, nazwyKolumn);
            try {
                usunPracownika(id);
                String [][] daneAktualizacja = databaseConnection.getPracownicy();
                tabelaAktualizacja = new JTable(daneAktualizacja, nazwyKolumn);
                scrollPaneUpdate = new JScrollPane(tabelaAktualizacja);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            } catch (ClassNotFoundException classNotFoundException) {
                classNotFoundException.printStackTrace();
            }
            scrollPane[0] = scrollPaneUpdate;
            scrollPane[0].getVerticalScrollBar().setPreferredSize(new Dimension(20, Integer.MAX_VALUE));
            tabela [0] = tabelaAktualizacja;
            panel.add(scrollPane[0]);
            panel.updateUI();
            frame.repaint();

        });

        frame.add(panel);
        frame.setMinimumSize(new Dimension(800, 600));
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

    ResultSet znajdzPracownika(String nazwisko) throws SQLException, ClassNotFoundException{
        try {
            DatabaseConnection databaseConnection = DatabaseConnection.getInstance();

            PreparedStatement preparedStatement;
            String query = "SELECT * FROM Pracownicy WHERE nazwisko_pracownika = ?";
            preparedStatement = databaseConnection.getConnection().prepareStatement(query);
            preparedStatement.setString(1, nazwisko);
            return preparedStatement.executeQuery();

        }
        catch (SQLException err) {
            JOptionPane.showMessageDialog(null, "Błąd SQL");
        }
        return null;
    }

    public void dodajPracownika() throws SQLException, ClassNotFoundException{
        JTextField imieField = new JTextField();
        JTextField nazwField = new JTextField();
        JTextField dataField = new JTextField();
        JTextField zarobkiField = new JTextField();
        JTextField telField = new JTextField();
        JPanel okienko = new JPanel(new GridLayout( 0, 1));

        okienko.add(new JLabel("Imię pracownika"));
        okienko.add(imieField);
        okienko.add(new JLabel("Nazwisko pracownika"));
        okienko.add(nazwField);
        okienko.add(new JLabel("Data zatrudnienia pracownika"));
        okienko.add(dataField);
        okienko.add(new JLabel("Zarobki pracownika"));
        okienko.add(zarobkiField);
        okienko.add(new JLabel("Numer telefonu"));
        okienko.add(telField);

        int wynik = JOptionPane.showConfirmDialog(null, okienko, "Dodaj pracownika ",
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if(wynik == JOptionPane.OK_OPTION)
        {
            try {
                int tel;
                String imie, nazwisko, data;
                double zarobki;
                if (!((imieField.getText().equals("")) && (nazwField.getText().equals("")) &&
                        (dataField.getText().equals("")) && (Double.parseDouble(zarobkiField.getText())) == 0 && Integer.parseInt(telField.getText()) == 0)) {

                    imie = imieField.getText();
                    nazwisko = nazwField.getText();
                    data = dataField.getText();
                    zarobki = Double.parseDouble(zarobkiField.getText());
                    tel = Integer.parseInt(telField.getText());

                    DatabaseConnection databaseConnection = DatabaseConnection.getInstance();

                    PreparedStatement preparedStatement;
                    String query = "INSERT INTO Pracownicy VALUES (?, ?, ?, ?, ?, ?)";
                    preparedStatement = databaseConnection.getConnection().prepareStatement(query);
                    preparedStatement.setString(2, imie);
                    preparedStatement.setString(3, nazwisko);
                    preparedStatement.setString(4, data);
                    preparedStatement.setDouble(5, zarobki);
                    preparedStatement.setInt(6, tel);
                    preparedStatement.executeUpdate();

                }

            }
            catch (SQLException err) {
                JOptionPane.showMessageDialog(null, "Błąd SQL");
            }
        }
        else{
            System.out.println("Anulowano");

        }
    }

    public void aktualizujPracownika(Object old_id, Object old_imie, Object old_nazw, Object old_data, Object old_zarobki, Object old_tel) throws SQLException, ClassNotFoundException {
        JTextField imieField = new JTextField(old_imie.toString());
        JTextField nazwiskoField = new JTextField(old_nazw.toString());
        JTextField dataField = new JTextField(old_data.toString());
        JTextField zarobkiField = new JTextField(old_zarobki.toString());
        JTextField telField = new JTextField(old_tel.toString());
        JPanel okienko = new JPanel(new GridLayout( 0, 1));

        okienko.add(new JLabel("Imię pracownika"));
        okienko.add(imieField);
        okienko.add(new JLabel("Nazwisko pracownika"));
        okienko.add(nazwiskoField);
        okienko.add(new JLabel("Data zatrudnienia pracownika"));
        okienko.add(dataField);
        okienko.add(new JLabel("Zarobki pracownika"));
        okienko.add(zarobkiField);
        okienko.add(new JLabel("Telefon pracownika"));
        okienko.add(telField);

        int wynik = JOptionPane.showConfirmDialog(null, okienko, "Aktualizuj pracownika ",
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if(wynik == JOptionPane.OK_OPTION)
        {
            try {
                String imie, nazwisko, data;
                int tel;
                double zarobki;
                if (!(imieField.getText().equals("") && nazwiskoField.getText().equals("")
                        && dataField.getText().equals("") && Double.parseDouble(zarobkiField.getText()) == 0 && Integer.parseInt(telField.getText()) == 0)) {
                    imie = imieField.getText();
                    nazwisko = nazwiskoField.getText();
                    data = dataField.getText();
                    zarobki = Double.parseDouble(zarobkiField.getText());
                    tel = Integer.parseInt(telField.getText());

                    DatabaseConnection databaseConnection = DatabaseConnection.getInstance();

                    PreparedStatement preparedStatement;
                    String query = "UPDATE Pracownicy SET imię = ?, nazwisko_pracownika = ?, data_zatrudnienia = ?, zarobki = ?, numer_telefonu = ? WHERE id_pracownika = "+ old_id;
                    preparedStatement = databaseConnection.getConnection().prepareStatement(query);
                    preparedStatement.setString(1, imie);
                    preparedStatement.setString(2, nazwisko);
                    preparedStatement.setString(3, data);
                    preparedStatement.setDouble(4, zarobki);
                    preparedStatement.setInt(5, tel);
                    preparedStatement.executeUpdate();
                }

            }
            catch (SQLException err) {
                JOptionPane.showMessageDialog(null, "Błąd SQL");
            }
        }
        else{
            System.out.println("Anulowano");

        }
    }
    public void usunPracownika(Object id) throws SQLException, ClassNotFoundException{
        DatabaseConnection databaseConnection = DatabaseConnection.getInstance();

        PreparedStatement preparedStatement;
        String query = "DELETE FROM Pracownicy WHERE id_pracownika = "+ id;
        preparedStatement = databaseConnection.getConnection().prepareStatement(query);
        preparedStatement.executeUpdate();
    }
}

