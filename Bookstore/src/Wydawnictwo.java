

import net.proteanit.sql.DbUtils;

import javax.swing.*;
import java.awt.*;
import java.sql.*;

public class Wydawnictwo {
    private Statement stmt;

    public void wydawnictwo() throws SQLException, ClassNotFoundException{
        DatabaseConnection databaseConnection = DatabaseConnection.getInstance();
        String[][] dane = databaseConnection.getWydawnictwa();
        String[] nazwyKolumn = {"ID", "nazwa_wydawnictwa"};
        final JTable[] tabela = {new JTable(dane, nazwyKolumn)};
        final JScrollPane[] scrollPane = {new JScrollPane(tabela[0])};
        tabela[0].setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        for (int i = 0; i < tabela[0].getColumnCount(); i++){
            tabela[0].getColumnModel().getColumn(i).setPreferredWidth(200);
        }
        tabela[0].setPreferredScrollableViewportSize(new Dimension(tabela[0].getPreferredSize().width, 300));
        scrollPane[0].getVerticalScrollBar().setPreferredSize(new Dimension(20, Integer.MAX_VALUE));

        JFrame frame = new JFrame("Wydawnictwa");
        JPanel panel = new JPanel();
        JPanel buttonPanel = new JPanel();
        JPanel przyciskPanel = new JPanel(new GridLayout(2, 3));
        JPanel szukajPanel = new JPanel();

        JButton powrotButton = new JButton("Powrót");
        JButton dodajButton = new JButton("Dodaj");
        JButton aktualizujButton = new JButton("Zmień");
        JButton usunButton = new JButton("Usuń");
        JButton znajdzButton = new JButton("Znajdź");
        JTextField znajdzTekst = new JTextField("", 20);
        JLabel znajdz = new JLabel("Wpisz nazwę");

        szukajPanel.add(znajdz);
        szukajPanel.add(znajdzTekst);
        szukajPanel.add(znajdzButton);

        buttonPanel.add(powrotButton);
        buttonPanel.add(dodajButton);
        buttonPanel.add(aktualizujButton);
        buttonPanel.add(usunButton);

        przyciskPanel.add(szukajPanel);
        przyciskPanel.add(buttonPanel);
        panel.add(przyciskPanel);
        panel.add(scrollPane[0]);

        powrotButton.addActionListener(e ->
        {
            frame.dispose();
            SwingUtilities.invokeLater(() -> new Main().Ekran());
        });

        znajdzButton.addActionListener(e ->
        {
            panel.remove(scrollPane[0]);
            JScrollPane scrollPaneAktualizacja = new JScrollPane(tabela[0]);
            JTable tabelaAktualizacja = new JTable(dane, nazwyKolumn);
            try {
                String tekst = znajdzTekst.getText();
                tabelaAktualizacja.setModel(DbUtils.resultSetToTableModel(znajdzWydawnictwo(tekst)));
                scrollPaneAktualizacja = new JScrollPane(tabelaAktualizacja);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            } catch (ClassNotFoundException classNotFoundException) {
                classNotFoundException.printStackTrace();
            }
            scrollPane[0] = scrollPaneAktualizacja;
            scrollPane[0].getVerticalScrollBar().setPreferredSize(new Dimension(20, Integer.MAX_VALUE));
            tabela[0] = tabelaAktualizacja;
            panel.add(scrollPane[0]);
            panel.updateUI();
            frame.repaint();
        });

        dodajButton.addActionListener(e ->
        {
            panel.remove(scrollPane[0]);
            JScrollPane scrollPaneUpdate = new JScrollPane(tabela[0]);
            JTable tabelaAktualizacja = new JTable(dane, nazwyKolumn);
            try {
                dodajWydawnictwo();
                String [][] daneAktualizacja = databaseConnection.getWydawnictwa();
                tabelaAktualizacja = new JTable(daneAktualizacja, nazwyKolumn);
                scrollPaneUpdate = new JScrollPane(tabelaAktualizacja);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            } catch (ClassNotFoundException classNotFoundException) {
                classNotFoundException.printStackTrace();
            }
            scrollPane[0] = scrollPaneUpdate;
            scrollPane[0].getVerticalScrollBar().setPreferredSize(new Dimension(20, Integer.MAX_VALUE));
            tabela[0] = tabelaAktualizacja;
            panel.add(scrollPane[0]);
            panel.updateUI();
            frame.repaint();

        });

        aktualizujButton.addActionListener(e ->
        {
            panel.remove(scrollPane[0]);
            JScrollPane scrollPaneUpdate = new JScrollPane(tabela[0]);
            JTable tabelaAktualizacja = new JTable(dane, nazwyKolumn);
            try {
                int rekord = tabela[0].getSelectedRow();
                Object old_id_wydawnictwa = tabela[0].getValueAt(rekord, 0);
                Object old_nazwa_wydawnictwa = tabela[0].getValueAt(rekord, 1);
                aktualizujWydawnictwo(old_id_wydawnictwa, old_nazwa_wydawnictwa);
                String [][] zaaktualizowaneDane = databaseConnection.getWydawnictwa();
                tabelaAktualizacja = new JTable(zaaktualizowaneDane, nazwyKolumn);
                scrollPaneUpdate = new JScrollPane(tabelaAktualizacja);


            } catch (SQLException throwables) {
                throwables.printStackTrace();
            } catch (ClassNotFoundException classNotFoundException) {
                classNotFoundException.printStackTrace();
            }
            scrollPane[0] = scrollPaneUpdate;
            scrollPane[0].getVerticalScrollBar().setPreferredSize(new Dimension(20, Integer.MAX_VALUE));
            tabela[0] = tabelaAktualizacja;
            panel.add(scrollPane[0]);
            panel.updateUI();
            frame.repaint();

        });

        usunButton.addActionListener(e ->
        {
            int rekord = tabela[0].getSelectedRow();
            Object id = tabela[0].getValueAt(rekord, 0);
            panel.remove(scrollPane[0]);
            JScrollPane scrollPaneUpdate = new JScrollPane(tabela[0]);
            JTable tabelaAktualizacja = new JTable(dane, nazwyKolumn);
            try {
                usunWydawnictwo(id);
                String [][] daneAktualizacja = databaseConnection.getWydawnictwa();
                tabelaAktualizacja = new JTable(daneAktualizacja, nazwyKolumn);
                scrollPaneUpdate = new JScrollPane(tabelaAktualizacja);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            } catch (ClassNotFoundException classNotFoundException) {
                classNotFoundException.printStackTrace();
            }
            scrollPane[0] = scrollPaneUpdate;
            scrollPane[0].getVerticalScrollBar().setPreferredSize(new Dimension(20, Integer.MAX_VALUE));
            tabela [0] = tabelaAktualizacja;
            panel.add(scrollPane[0]);
            panel.updateUI();
            frame.repaint();

        });

        frame.add(panel);
        frame.setMinimumSize(new Dimension(600, 600));
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

    ResultSet znajdzWydawnictwo(String nazwa_wydawnictwa) throws SQLException, ClassNotFoundException{
        try {

                DatabaseConnection databaseConnection = DatabaseConnection.getInstance();
                PreparedStatement preparedStatement;
                String query = "SELECT nazwa_wydawnictwa FROM Wydawnictwo WHERE nazwa_wydawnictwa = ?";
                preparedStatement = databaseConnection.getConnection().prepareStatement(query);
                preparedStatement.setString(1, nazwa_wydawnictwa);
                return preparedStatement.executeQuery();

            }
        catch (SQLException err) {
            JOptionPane.showMessageDialog(null, "Błąd SQL");
        }
        return null;
    }

    public void dodajWydawnictwo() throws SQLException, ClassNotFoundException{
        JTextField id_wydawnictwaField = new JTextField();
        JTextField nazwa_wydawnictwaField = new JTextField();
        JPanel okienko = new JPanel(new GridLayout( 0, 1));

        okienko.add(new JLabel("Nazwa wydawnictwa"));
        okienko.add(nazwa_wydawnictwaField);

        int wynik = JOptionPane.showConfirmDialog(null, okienko, "Dodaj wydawnictwo ",
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if(wynik == JOptionPane.OK_OPTION)
        {
            try {
                int id_wydawnictwa;
                String nazwa_wydawnictwa;
                if (!((nazwa_wydawnictwaField.getText().equals("")))) {
                    nazwa_wydawnictwa = nazwa_wydawnictwaField.getText();

                    DatabaseConnection databaseConnection = DatabaseConnection.getInstance();

                    PreparedStatement preparedStatement;
                    String query = "INSERT INTO Wydawnictwo VALUES (?, ?)";
                    preparedStatement = databaseConnection.getConnection().prepareStatement(query);
                    preparedStatement.setString(2, nazwa_wydawnictwa);
                    preparedStatement.executeUpdate();
                }

            }
            catch (SQLException err) {
                JOptionPane.showMessageDialog(null, "Błąd SQL");
            }
        }
        else{
            System.out.println("Anulowano");

        }
    }

    public void aktualizujWydawnictwo(Object old_id, Object old_nazwa) throws SQLException, ClassNotFoundException {
        JTextField nazwa_wydawnictwaField = new JTextField(old_nazwa.toString());
        JPanel okienko = new JPanel(new GridLayout( 0, 1));

        okienko.add(new JLabel("Nazwa wydawnictwa"));
        okienko.add(nazwa_wydawnictwaField);

        int wynik = JOptionPane.showConfirmDialog(null, okienko, "Aktualizuj wydawnictwo ",
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if(wynik == JOptionPane.OK_OPTION)
        {
            try {
                String nazwa_wydawnictwa;
                if (!(nazwa_wydawnictwaField.getText().equals(""))) {
                    nazwa_wydawnictwa = nazwa_wydawnictwaField.getText();

                    DatabaseConnection databaseConnection = DatabaseConnection.getInstance();

                    PreparedStatement preparedStatement;
                    String query = "UPDATE Wydawnictwo SET nazwa_wydawnictwa = ? WHERE id_wydawnictwa = "+ old_id;
                    preparedStatement = databaseConnection.getConnection().prepareStatement(query);
                    preparedStatement.setString(1, nazwa_wydawnictwa);
                    preparedStatement.executeUpdate();
                }

            }
            catch (SQLException err) {
                JOptionPane.showMessageDialog(null, "Błąd SQL");
            }
        }
        else{
            System.out.println("Anulowano");

        }
    }
    public void usunWydawnictwo(Object id) throws SQLException, ClassNotFoundException{
        DatabaseConnection databaseConnection = DatabaseConnection.getInstance();

        PreparedStatement preparedStatement;
        String query = "DELETE FROM Wydawnictwo WHERE id_wydawnictwa = "+ id;
        preparedStatement = databaseConnection.getConnection().prepareStatement(query);
        preparedStatement.executeUpdate();
    }
}
