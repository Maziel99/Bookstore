import java.sql.*;

public class DatabaseConnection {

    private static DatabaseConnection instance;
    private Connection connection;
    private String url = "org.sqlite.JDBC";
    private Statement stmt;

    private DatabaseConnection() throws SQLException {
        try {
            String url = "jdbc:sqlite:Ksiegarnia.db3";
            Class.forName("org.sqlite.JDBC");
            this.connection = DriverManager.getConnection(url);

        } catch (ClassNotFoundException ex) {
            System.out.println("Połączenie z bazą nieudane : " + ex.getMessage());
        }
    }

    public Connection getConnection() {
        return connection;
    }

    public static DatabaseConnection getInstance() throws SQLException {
        if (instance == null) {
            instance = new DatabaseConnection();
        } else if (instance.getConnection().isClosed()) {
            instance = new DatabaseConnection();
        }

        return instance;

    }

    public String[][] getWydawnictwa() throws ClassNotFoundException, SQLException {
        DatabaseConnection databaseConnection = DatabaseConnection.getInstance();
        databaseConnection.getConnection();
        Statement stmt = connection.createStatement();
        ResultSet result = stmt.executeQuery("SELECT * FROM Wydawnictwo");
        ResultSetMetaData meta = result.getMetaData();
        String[][] dane = new String[50][meta.getColumnCount() + 1];
        int j = 0, i;
        while (result.next()) {
            i = 0;
            while (i < meta.getColumnCount()) {
                dane[j][i] = result.getString(i + 1);
                i++;
            }
            System.out.println();
            j++;
        }
        return dane;
    }

    public String[][] getWypozyczenia() throws ClassNotFoundException, SQLException {
        DatabaseConnection databaseConnection = DatabaseConnection.getInstance();
        databaseConnection.getConnection();
        Statement stmt = connection.createStatement();
        ResultSet result = stmt.executeQuery("SELECT * FROM Wypozyczenia");
        ResultSetMetaData meta = result.getMetaData();
        String[][] dane = new String[50][meta.getColumnCount() + 1];
        int j = 0, i;
        while (result.next()) {
            i = 0;
            while (i < meta.getColumnCount()) {
                dane[j][i] = result.getString(i + 1);
                i++;
            }
            System.out.println();
            j++;
        }
        return dane;
    }

    public String[][] getWypozyczający() throws ClassNotFoundException, SQLException {
        DatabaseConnection databaseConnection = DatabaseConnection.getInstance();
        databaseConnection.getConnection();
        Statement stmt = connection.createStatement();
        ResultSet result = stmt.executeQuery("SELECT * FROM Wypożyczający");
        ResultSetMetaData meta = result.getMetaData();
        String[][] dane = new String[50][meta.getColumnCount() + 1];
        int j = 0, i;
        while (result.next()) {
            i = 0;
            while (i < meta.getColumnCount()) {
                dane[j][i] = result.getString(i + 1);
                i++;
            }
            System.out.println();
            j++;
        }
        return dane;
    }

    public String[][] getKsiazki() throws ClassNotFoundException, SQLException {
        DatabaseConnection databaseConnection = DatabaseConnection.getInstance();
        databaseConnection.getConnection();
        Statement stmt = connection.createStatement();
        ResultSet result = stmt.executeQuery("SELECT * FROM Książki");
        ResultSetMetaData meta = result.getMetaData();
        String[][] dane = new String[50][meta.getColumnCount() + 1];
        int j = 0, i;
        while (result.next()) {
            i = 0;
            while (i < meta.getColumnCount()) {
                dane[j][i] = result.getString(i + 1);
                i++;
            }
            System.out.println();
            j++;
        }
        return dane;
    }

    public String[][] getPracownicy() throws ClassNotFoundException, SQLException {
        DatabaseConnection databaseConnection = DatabaseConnection.getInstance();
        databaseConnection.getConnection();
        Statement stmt = connection.createStatement();
        ResultSet result = stmt.executeQuery("SELECT * FROM Pracownicy");
        ResultSetMetaData meta = result.getMetaData();
        String[][] dane = new String[30][meta.getColumnCount() + 1];
        int j = 0, i;
        while (result.next()) {
            i = 0;
            while (i < meta.getColumnCount()) {
                dane[j][i] = result.getString(i + 1);
                i++;
            }
            System.out.println();
            j++;
        }
        return dane;
    }

    public String[] getWyd() throws ClassNotFoundException, SQLException {
        DatabaseConnection databaseConnection = DatabaseConnection.getInstance();
        databaseConnection.getConnection();
        Statement stmt = connection.createStatement();
        ResultSet result = stmt.executeQuery("SELECT nazwa_wydawnictwa FROM Wydawnictwo");
        ResultSetMetaData meta = result.getMetaData();
        String[] dane = new String[30];
        int i = 0;
        while (result.next()) {
            dane[i] = result.getString("nazwa_wydawnictwa");
            i++;
        }

        return dane;


    }

    public String[] getWyp() throws ClassNotFoundException, SQLException {
        DatabaseConnection databaseConnection = DatabaseConnection.getInstance();
        databaseConnection.getConnection();
        Statement stmt = connection.createStatement();
        ResultSet result = stmt.executeQuery("SELECT nr_karty FROM Wypożyczający");
        ResultSetMetaData meta = result.getMetaData();
        String[] dane = new String[30];
        int i = 0;
        while (result.next()) {
            dane[i] = result.getString("nr_karty");
            i++;
        }

        return dane;


    }

    public String[] getPrac() throws ClassNotFoundException, SQLException {
        DatabaseConnection databaseConnection = DatabaseConnection.getInstance();
        databaseConnection.getConnection();
        Statement stmt = connection.createStatement();
        ResultSet result = stmt.executeQuery("SELECT nazwisko_pracownika FROM Pracownicy");
        ResultSetMetaData meta = result.getMetaData();
        String[] dane = new String[30];
        int i = 0;
        while (result.next()) {
            dane[i] = result.getString("nazwisko_pracownika");
            i++;
        }

        return dane;


    }

    public String[] getTyt() throws ClassNotFoundException, SQLException {
        DatabaseConnection databaseConnection = DatabaseConnection.getInstance();
        databaseConnection.getConnection();
        Statement stmt = connection.createStatement();
        ResultSet result = stmt.executeQuery("SELECT tytuł FROM Książki");
        ResultSetMetaData meta = result.getMetaData();
        String[] dane = new String[30];
        int i = 0;
        while (result.next()) {
            dane[i] = result.getString("tytuł");
            i++;
        }

        return dane;


    }
}
