import net.proteanit.sql.DbUtils;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;

import javax.swing.*;
import java.awt.*;
import java.sql.*;

public class Wypozyczajacy {

    public void wypozyczajacy() throws SQLException, ClassNotFoundException{
        DatabaseConnection databaseConnection = DatabaseConnection.getInstance();
        String[][] dane = databaseConnection.getWypozyczający();
        String[] nazwyKolumn = {"ID", "imię", "nazwisko","nr_telefonu", "nr_karty"};
        final JTable[] tabela = {new JTable(dane, nazwyKolumn)};
        final JScrollPane[] scrollPane = {new JScrollPane(tabela[0])};
        tabela[0].setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        for (int i = 0; i < tabela[0].getColumnCount(); i++){
            tabela[0].getColumnModel().getColumn(i).setPreferredWidth(130);
        }
        tabela[0].setPreferredScrollableViewportSize(new Dimension(tabela[0].getPreferredSize().width, 300));
        scrollPane[0].getVerticalScrollBar().setPreferredSize(new Dimension(20, Integer.MAX_VALUE));
        JFrame frame = new JFrame("Wypożyczający");
        JPanel panel = new JPanel();
        JPanel buttonPanel = new JPanel();
        JPanel przyciskPanel = new JPanel(new GridLayout(2, 3));
        JPanel szukajPanel = new JPanel();

        JButton powrotButton = new JButton("Powrót");
        JButton dodajButton = new JButton("Dodaj");
        JButton aktualizujButton = new JButton("Zmień");
        JButton usunButton = new JButton("Usuń");
        JButton znajdzButton = new JButton("Znajdź");
        JTextField znajdzTekst = new JTextField("", 20);
        JLabel znajdz = new JLabel("Wpisz nazwisko");

        szukajPanel.add(znajdz);
        szukajPanel.add(znajdzTekst);
        szukajPanel.add(znajdzButton);

        buttonPanel.add(powrotButton);
        buttonPanel.add(dodajButton);
        buttonPanel.add(aktualizujButton);
        buttonPanel.add(usunButton);

        przyciskPanel.add(szukajPanel);
        przyciskPanel.add(buttonPanel);
        panel.add(przyciskPanel);
        panel.add(scrollPane[0]);

        powrotButton.addActionListener(e ->
        {
            frame.dispose();
            SwingUtilities.invokeLater(() -> new Main().Ekran());
        });

        znajdzButton.addActionListener(e ->
        {
            panel.remove(scrollPane[0]);
            JScrollPane scrollPaneAktualizacja = new JScrollPane(tabela[0]);
            JTable tabelaAktualizacja = new JTable(dane, nazwyKolumn);
            try {
                String tekst = znajdzTekst.getText();
                tabelaAktualizacja.setModel(DbUtils.resultSetToTableModel(znajdzWypozyczajacego(tekst)));
                scrollPaneAktualizacja = new JScrollPane(tabelaAktualizacja);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            } catch (ClassNotFoundException classNotFoundException) {
                classNotFoundException.printStackTrace();
            }
            scrollPane[0] = scrollPaneAktualizacja;
            scrollPane[0].getVerticalScrollBar().setPreferredSize(new Dimension(20, Integer.MAX_VALUE));
            tabela[0] = tabelaAktualizacja;
            tabela[0].setPreferredScrollableViewportSize(new Dimension(tabela[0].getPreferredSize().width, 300));
            panel.add(scrollPane[0]);
            panel.updateUI();
            frame.repaint();
        });

        dodajButton.addActionListener(e ->
        {
            panel.remove(scrollPane[0]);
            JScrollPane scrollPaneUpdate = new JScrollPane(tabela[0]);
            JTable tabelaAktualizacja = new JTable(dane, nazwyKolumn);
            try {
                dodajWypozyczajacego();
                String [][] daneAktualizacja = databaseConnection.getWypozyczający();
                tabelaAktualizacja = new JTable(daneAktualizacja, nazwyKolumn);
                scrollPaneUpdate = new JScrollPane(tabelaAktualizacja);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            } catch (ClassNotFoundException classNotFoundException) {
                classNotFoundException.printStackTrace();
            }
            scrollPane[0] = scrollPaneUpdate;
            scrollPane[0].getVerticalScrollBar().setPreferredSize(new Dimension(20, Integer.MAX_VALUE));
            tabela[0] = tabelaAktualizacja;
            panel.add(scrollPane[0]);
            panel.updateUI();
            frame.repaint();

        });

        aktualizujButton.addActionListener(e ->
        {
            panel.remove(scrollPane[0]);
            JScrollPane scrollPaneUpdate = new JScrollPane(tabela[0]);
            JTable tabelaAktualizacja = new JTable(dane, nazwyKolumn);
            try {
                int rekord = tabela[0].getSelectedRow();
                Object old_id_wyp = tabela[0].getValueAt(rekord, 0);
                Object old_imie = tabela[0].getValueAt(rekord, 1);
                Object old_nazwisko = tabela[0].getValueAt(rekord, 2);
                Object old_telefon = tabela[0].getValueAt(rekord, 3);
                Object old_karta = tabela[0].getValueAt(rekord, 4);
                aktualizujWypozyczajacego(old_id_wyp, old_imie, old_nazwisko, old_telefon, old_karta);
                String [][] zaaktualizowaneDane = databaseConnection.getWypozyczający();
                tabelaAktualizacja = new JTable(zaaktualizowaneDane, nazwyKolumn);
                scrollPaneUpdate = new JScrollPane(tabelaAktualizacja);


            } catch (SQLException throwables) {
                throwables.printStackTrace();
            } catch (ClassNotFoundException classNotFoundException) {
                classNotFoundException.printStackTrace();
            }
            scrollPane[0] = scrollPaneUpdate;
            scrollPane[0].getVerticalScrollBar().setPreferredSize(new Dimension(20, Integer.MAX_VALUE));
            tabela[0] = tabelaAktualizacja;
            panel.add(scrollPane[0]);
            panel.updateUI();
            frame.repaint();

        });

        usunButton.addActionListener(e ->
        {
            int rekord = tabela[0].getSelectedRow();
            Object id = tabela[0].getValueAt(rekord, 0);
            panel.remove(scrollPane[0]);
            JScrollPane scrollPaneUpdate = new JScrollPane(tabela[0]);
            JTable tabelaAktualizacja = new JTable(dane, nazwyKolumn);
            try {
                usunWypozyczajacego(id);
                String [][] daneAktualizacja = databaseConnection.getWypozyczający();
                tabelaAktualizacja = new JTable(daneAktualizacja, nazwyKolumn);
                scrollPaneUpdate = new JScrollPane(tabelaAktualizacja);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            } catch (ClassNotFoundException classNotFoundException) {
                classNotFoundException.printStackTrace();
            }
            scrollPane[0] = scrollPaneUpdate;
            scrollPane[0].getVerticalScrollBar().setPreferredSize(new Dimension(20, Integer.MAX_VALUE));
            tabela [0] = tabelaAktualizacja;
            panel.add(scrollPane[0]);
            panel.updateUI();
            frame.repaint();

        });

        frame.add(panel);
        frame.setMinimumSize(new Dimension(700, 600));
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

    ResultSet znajdzWypozyczajacego(String nazwisko) throws SQLException, ClassNotFoundException{
        try {
            DatabaseConnection databaseConnection = DatabaseConnection.getInstance();

            PreparedStatement preparedStatement;
            String query = "SELECT * FROM Wypożyczający WHERE nazwisko = ?";
            preparedStatement = databaseConnection.getConnection().prepareStatement(query);
            preparedStatement.setString(1, nazwisko);
            return preparedStatement.executeQuery();

        }
        catch (SQLException err) {
            JOptionPane.showMessageDialog(null, "Błąd SQL");
        }
        return null;
    }

    public void dodajWypozyczajacego() throws SQLException, ClassNotFoundException{

        DatabaseConnection databaseConnection = DatabaseConnection.getInstance();

        JTextField imieField = new JTextField();
        JTextField nazwiskoField = new JTextField();
        JTextField telField = new JTextField();
        JTextField kartaField = new JTextField();
        JPanel okienko = new JPanel(new GridLayout( 0, 1));

        okienko.add(new JLabel("Imię wypozyczającego"));
        okienko.add(imieField);
        okienko.add(new JLabel("Nazwisko wypożyczającego"));
        okienko.add(nazwiskoField);
        okienko.add(new JLabel("Telefon"));
        okienko.add(telField);
        okienko.add(new JLabel("Numer karty"));
        okienko.add(kartaField);

        int wynik = JOptionPane.showConfirmDialog(null, okienko, "Dodaj wypożyczającego ",
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if(wynik == JOptionPane.OK_OPTION)
        {
            try {
                int  karta, tel;
                String imie, nazwisko;
                if (!(imieField.getText().equals("") && nazwiskoField.getText().equals("")
                        && Integer.parseInt(telField.getText()) == 0 && Integer.parseInt(kartaField.getText()) == 0)) {

                    imie = imieField.getText();
                    nazwisko = nazwiskoField.getText();
                    tel = Integer.parseInt(telField.getText());
                    karta = Integer.parseInt(kartaField.getText());



                    PreparedStatement preparedStatement;
                    String query = "INSERT INTO Wypożyczający VALUES (?, ?, ?, ?, ?)";
                    preparedStatement = databaseConnection.getConnection().prepareStatement(query);
                    preparedStatement.setString(2, imie);
                    preparedStatement.setString(3, nazwisko);
                    preparedStatement.setInt(4, tel);
                    preparedStatement.setInt(5, karta);
                    preparedStatement.executeUpdate();

                }

            }
            catch (SQLException err) {
                JOptionPane.showMessageDialog(null, "Błąd SQL");
            }
        }
        else{
            System.out.println("Anulowano");

        }
    }

    public void aktualizujWypozyczajacego(Object old_id, Object old_imie, Object old_nazw, Object old_tel, Object old_kart) throws SQLException, ClassNotFoundException {
        JTextField imieField = new JTextField(old_imie.toString());
        JTextField nazwiskoField = new JTextField(old_nazw.toString());
        JTextField telField = new JTextField(old_tel.toString());
        JTextField kartaField = new JTextField(old_kart.toString());
        JPanel okienko = new JPanel(new GridLayout( 0, 1));

        okienko.add(new JLabel("Imię wypożyczającego"));
        okienko.add(imieField);
        okienko.add(new JLabel("Nazwisko wypożyczającego"));
        okienko.add(nazwiskoField);
        okienko.add(new JLabel("Telefon wypożyczającego"));
        okienko.add(telField);
        okienko.add(new JLabel("Numer karty"));
        okienko.add(kartaField);

        int wynik = JOptionPane.showConfirmDialog(null, okienko, "Aktualizuj wypożyczającego ",
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if(wynik == JOptionPane.OK_OPTION)
        {
            try {
                String imie, nazwisko;
                int tel, karta;
                if (!(imieField.getText().equals("") && nazwiskoField.getText().equals("")
                        && Integer.parseInt(telField.getText()) == 0 && Integer.parseInt(kartaField.getText()) == 0)) {
                    imie = imieField.getText();
                    nazwisko = nazwiskoField.getText();
                    tel = Integer.parseInt(telField.getText());
                    karta = Integer.parseInt(kartaField.getText());

                    DatabaseConnection databaseConnection = DatabaseConnection.getInstance();

                    PreparedStatement preparedStatement;
                    String query = "UPDATE Wypożyczający SET imię = ?, nazwisko = ?, nr_telefonu = ?, nr_karty = ? WHERE id_wypożyczającego = "+ old_id;
                    preparedStatement = databaseConnection.getConnection().prepareStatement(query);
                    preparedStatement.setString(1, imie);
                    preparedStatement.setString(2, nazwisko);
                    preparedStatement.setInt(3, tel);
                    preparedStatement.setInt(4, karta);
                    preparedStatement.executeUpdate();
                }

            }
            catch (SQLException err) {
                JOptionPane.showMessageDialog(null, "Błąd SQL");
            }
        }
        else{
            System.out.println("Anulowano");

        }
    }
    public void usunWypozyczajacego(Object id) throws SQLException, ClassNotFoundException{
        DatabaseConnection databaseConnection = DatabaseConnection.getInstance();

        PreparedStatement preparedStatement;
        String query = "DELETE FROM Wypożyczający WHERE id_wypożyczającego = "+ id;
        preparedStatement = databaseConnection.getConnection().prepareStatement(query);
        preparedStatement.executeUpdate();
    }
}